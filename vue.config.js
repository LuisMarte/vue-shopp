const path = require('path')

module.exports = {
  chainWebpack: config => {
    const apiClient = process.env.VUE_APP_API_CLIENT // mock or server
    config.resolve.alias.set(
      'api-client',
      path.resolve(__dirname, `src/api/${apiClient}`)
    )
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `
          @import "@/assets/sass/_variables.scss";
		  @import "@/assets/sass/_mixins.scss";
		  @import "@/assets/sass/_fonts.scss";
        `
      }
    }
  }
}
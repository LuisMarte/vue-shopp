# Vue Shopp

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
### Production server
After running npm run build, 'dist' directory is created with all the files required to deploy the app. 
For preview it you need to use a Node.js static file server. 
If you don't know how to do it just follow the [Vue CLI deployment guidelines](https://cli.vuejs.org/guide/deployment.html#general-guidelines).

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



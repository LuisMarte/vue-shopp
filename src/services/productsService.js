import service from './service';

const resource = 'products';

export default {
	get(){
		return service.get(resource)
	}
}


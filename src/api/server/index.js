import axios from 'axios'

export default {
  fetchProducts () {
    return axios
      .get('https://fakestoreapi.com/products')
      .then(response => response.data)
  }
}
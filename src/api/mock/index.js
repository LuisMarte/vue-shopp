import products from './data/products'

const fetch = (mockData, time = 0) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mockData)
    }, time)
  })
}

export default {
  fetchProducts () {
    return fetch(products, 200) // wait 0.2s before returning posts
  }
}

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './assets/sass/main.scss'
Vue.component('Icons', require('./components/icons/Icons.vue').default);

Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
